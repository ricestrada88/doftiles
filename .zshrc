# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
### correr el siguiente comando sudo pacman -Syu zsh zsh-autosuggestions zsh-completions zsh-history-substring-search zsh-syntax-highlighting
### correr el siguiente comando yay -S zsh-theme-powerlevel10k-git
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
# 256 color mode
export TERM="xterm-256color"
#history con fecha
#export HISTTIMEFORMAT="%h/%d - %H:%M:%S " solo funciona con bash <-- buscar la manera de hacerlo en zsh
# Path to your oh-my-zsh installation. // si se instala desde git el path esta en el home
export ZSH=/usr/share/oh-my-zsh

#path de oh-my-zsh
source /usr/share/oh-my-zsh/oh-my-zsh.sh
#path para las autosugerencias instaladas desde aur
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
## crear comando help
autoload -U run-help
autoload run-help-git
autoload run-help-svn
autoload run-help-svk
unalias run-help 2> /dev/null
alias help=run-help
#tema para zsh
ZSH_THEME="robbyrussell" # <-- jugar con los otros temas y coloschemes quizas pueda quedar mejor. Si no, se comenta total, el tema es powerlevel10k
#plugins
plugins=(git adb colored-man-pages wakeonlan ssh-agent)  
###completions desde AUR ya aparece en el $fpath, si no apareciere añadir al fpath
#### comando no encontrado, requiere instalar pkgfile
#source /usr/share/doc/pkgfile/command-not-found.zsh
###forzar el rehash
zstyle ':completion:*' rehash true

#powerlevel10k desde AUR nota: instalar la version git, lo recomienda el desarrollador
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
##alias basicos + alias lsd 
#alias ls='ls --color=auto'
#unalias lsd  
alias ls='lsd'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias diff='diff --color=auto'
alias history='history -i'
##goodies para manpages
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'
#alias para el neofetch
alias neofetch="neofetch | lolcat"
#alias del neovim
alias vim="nvim"
#COD autocomplete
##descargar el paquete e instalar como se indica: sudo install cod /usr/local/bin
source <(cod init $$ zsh)
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
##plugins que deben ir al final
#zsh resaltado de sintaxys desde repo // se debe de cargar al final
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#zsh-history desde repo // se debe de cargar despues del resaltado de sintaxys
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
######nnn config test #############
alias nnn='nnn -de' # -d for details and -e to open files in $VISUAL (for other options, see 'man nnn'...)
#-----
export NNN_OPTS="H" # 'H' shows the hidden files. Same as option -H (so 'nnn -deH')
# export NNN_OPTS="deH" # if you prefer to have all the options at the same place
export LC_COLLATE="C" # hidden files on top
export NNN_FIFO="/tmp/nnn.fifo" # temporary buffer for the previews
export NNN_FCOLORS="AAAAE631BBBBCCCCDDDD9999" # feel free to change the colors
export NNN_PLUG='p:preview-tui' # many other plugins are available here: https://github.com/jarun/nnn/tree/master/plugins
export SPLIT='v' # to split Kitty vertically
#-----
n () # to cd on quit
{
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    nnn "$@"
    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}
####VTE para tilix
if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi
