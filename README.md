# Doftiles

# zsh

## Run Gogh for the colorschemes i prefertha that than the AUR tilix coloschemes
~~~
bash -c  "$(wget -qO- https://git.io/vQgMr)" 
~~~
#
## Fig may be an interesting addon this let's the term an IDE like autocomplete

https://fig.io/user-manual/linux

To mas it work with arch install the AUR package fig, the other fig-headless not stillworkins to me

## Arch linux by repo packages
~~~
pacman -S zsh-theme-powerlevel10k ### theme for zsh with powerline
pacman -S zsh-syntax-highlighting  ### syntax highlight for the terminal
pacman -S zsh-history-substring-search ### history search feature <-- aprender a usarla mejor
pacman -S nnn ### powerfull terminal file manager
pacman -S fzf ### a general-purpose command-line fuzzy finder
yay -S cod-git ### a completion daemon for bash/fish/zsh
yay -S oh-my-zsh-git ### just oh-my-szh
yay -S zsh-autosuggestions ### fish like autosuggestions // may be it will be switched by fig
~~~

### Another intesting stuff to try
Informative git prompt for zsh:

https://github.com/olivierverdier/zsh-git-prompt

Marker is a command palette for the terminal. It lets you bookmark commands (or commands templates) and easily retreive them with the help of a real-time fuzzy matcher:
https://github.com/pindexis/marker
